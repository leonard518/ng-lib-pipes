import { NgModule } from '@angular/core';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
  exports: [
    PipesModule
  ]
})
export class DemoPipesModule { }
