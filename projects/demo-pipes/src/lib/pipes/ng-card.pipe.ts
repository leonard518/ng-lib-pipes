import { Pipe, PipeTransform, NgModule } from '@angular/core';

@Pipe({
  name: 'ngCard'
})

export class NgCardPipe implements PipeTransform {

  transform(value: string): string {
    return value
      .replace(/\s+/g, '')
      .replace(/(\d{4})/g, '$1 ')
      .trim();
  }

}


@NgModule({
  declarations: [NgCardPipe],
  exports: [NgCardPipe],
})

export class NgCardModule {}
