import { Pipe, PipeTransform, NgModule } from '@angular/core';


@Pipe({
  name: 'ngCurrency'
})

export class NgCurrencyPipe implements PipeTransform {

  transform(amount: number, decimals?: number, symbol?: string): string {
    decimals = decimals != null ? decimals : 2;
    symbol = symbol != null ? symbol : '$';

    return symbol + ' ' + amount
      .toFixed(decimals)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }

}

@NgModule({
  declarations: [NgCurrencyPipe],
  exports: [NgCurrencyPipe],
})
export class NgCurrencyModule {}
