import {NgModule, Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name: 'ngUppercase'
})

export class NgUppercasePipe implements PipeTransform {

  transform(content: string) {

    return content.toUpperCase();
  }

}

@NgModule({
  declarations: [
    NgUppercasePipe
  ],
  exports: [
    NgUppercasePipe
  ]
})

export class NgUppercaseModule { }
