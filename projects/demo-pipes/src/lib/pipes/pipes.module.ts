import { NgCurrencyModule } from './ng-currency.pipe';
import { NgModule } from '@angular/core';
import { NgCardModule } from './ng-card.pipe';
import { NgUppercaseModule } from './ng-uppercase.pipe';


@NgModule({
  imports: [
    NgCurrencyModule,
    NgUppercaseModule,
    NgCardModule
  ],
  exports: [
    NgCurrencyModule,
    NgUppercaseModule,
    NgCardModule
  ]
})
export class PipesModule { }
