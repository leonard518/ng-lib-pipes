/*
 * Public API Surface of demo-pipes
 */

export * from './lib/demo-pipes.module';

export * from './lib/pipes/pipes.module';
export * from './lib/pipes/ng-currency.pipe'
export * from './lib/pipes/ng-card.pipe'
export * from './lib/pipes/ng-uppercase.pipe'
